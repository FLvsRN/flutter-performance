import 'package:flutter/material.dart';
import 'dart:math';

import 'package:performance/calc.dart';

const List<int> _input = [1, 7, 12, 23, 37, 40, 42];
const int _iterations = 10;

class App extends StatefulWidget {
  App({Key key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  var _res1;
  var _res2;
  var _res3;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          body: Padding(
        padding: EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ElevatedButton(
                child: Container(
                    margin: EdgeInsets.all(5.0),
                    child: Text(
                      "Fibonacci\n(Binet's formula)",
                      textAlign: TextAlign.center,
                    )),
                onPressed: () {
                  pressFibonacciBinetsFormula();
                }),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(_res1 ?? ''),
            ),
            Divider(),
            ElevatedButton(
                child: Container(
                    margin: EdgeInsets.all(5.0),
                    child: Text(
                      'Fibonacci\n(reqursion)',
                      textAlign: TextAlign.center,
                    )),
                onPressed: () {
                  pressFibonacciRecursive();
                }),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(_res2 ?? ''),
            ),
            Divider(),
            ElevatedButton(
                child: Container(
                    margin: EdgeInsets.all(5.0),
                    child: Text(
                      'Fibonacci\n(reqursion + memorization)',
                      textAlign: TextAlign.center,
                    )),
                onPressed: () {
                  pressFibonacciRecursiveMemorization();
                }),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(_res3 ?? ''),
            ),
            Divider(),
          ],
        ),
      )),
    );
  }

  pressFibonacciBinetsFormula() async {
    final Stopwatch watch = Stopwatch();
    print('#1 tracker benchmark...');

    watch.start();
    for (var i = 0; i < _iterations; i++) {
      await runFibonacciBinetsFormula();
    }
    watch.stop();

    final execTime = watch.elapsedMicroseconds;
    final res = _roundDouble((execTime / (_input.length * _iterations)), 4);

    setState(() {
      _res1 = '$res μs';
    });
    print('avg time per iteration $res μs');
  }

  pressFibonacciRecursive() {
    final Stopwatch watch = Stopwatch();
    print('#2 tracker benchmark...');

    watch.start();
    for (var i = 0; i < _iterations; i++) {
      print('>> $i');
      runFibonacciRecursive();
    }
    watch.stop();

    final execTime = watch.elapsedMilliseconds;
    print(execTime);
    final res = _roundDouble((execTime / (_input.length * _iterations)), 4);
    setState(() {
      _res2 = '$res ms';
    });
    print('avg time per iteration $res ms');
  }

  pressFibonacciRecursiveMemorization() {
    final Stopwatch watch = Stopwatch();
    print('#3 tracker benchmark...');
    watch.start();
    for (var i = 0; i < _iterations; i++) {
      runFibonacciRecursiveMemorization();
    }
    watch.stop();

    final execTime = watch.elapsedMicroseconds;
    final res = _roundDouble((execTime / (_input.length * _iterations)), 4);

    setState(() {
      _res3 = '$res μs';
    });
    print('avg time per iteration $res μs');
  }

  runFibonacciBinetsFormula() {
    Calc calc = Calc();
    for (var item in _input) {
      calc.fibonacciBinetsFormula(item);
    }
  }

  runFibonacciRecursive() {
    Calc calc = Calc();
    for (var item in _input) {
      calc.fibonacciRecursive(item);
    }
  }

  runFibonacciRecursiveMemorization() {
    Calc calc = Calc();
    for (var item in _input) {
      calc.fibonacciRecursiveMemorization(item);
    }
  }
}

double _roundDouble(double value, int places) {
  double mod = pow(10.0, places);
  return ((value * mod).round().toDouble() / mod);
}
