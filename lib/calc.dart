import 'dart:math';

class Calc {
  int fibonacciBinetsFormula(int n) =>
      ((pow(1 + sqrt(5), n) - pow(1 - sqrt(5), n)) / (pow(2, n) * sqrt(5)))
          .round();

  int fibonacciRecursive(int n) {
    if (n == 0)
      return 0;
    else if (n == 1)
      return 1;
    else
      return fibonacciRecursive(n - 1) + fibonacciRecursive(n - 2);
  }

  int fibonacciRecursiveMemorization(int n) {
    var cache = [0, 1];
    fibonacciRecursiveArray(int n) {
      if (n == 0) return 0;
      if (n == 1)
        return 1;
      else if (n <= cache.length) {
        return cache[n - 1];
      } else {
        var temp =
            fibonacciRecursiveArray(n - 1) + fibonacciRecursiveArray(n - 2);
        cache.add(temp);
        return temp;
      }
    }

    return fibonacciRecursiveArray(n);
  }
}
